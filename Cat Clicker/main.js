var Cat;
var frame = 0;
var Score = 0;
var perSec = 0;
var time = 0;
var shopMark;
var isShowVisible = false;
var PrefMouseIsPressed = false;
var sounds = [];
var stars = [];
var speed;

function preload() {
	Cat = loadAnimation('fCat1.png', 'fCat2.png');
	Cat.looping = false;
	soundFormats('mp3');
	sounds.push(loadSound('sounds/chalk.mp3'));
	sounds.push(loadSound('sounds/drill.mp3'));
	sounds.push(loadSound('sounds/car.mp3'));
	sounds.push(loadSound('sounds/rocket.mp3'));
	sounds.push(loadSound('sounds/despasito.mp3'));
}

function setup() {
	createCanvas(1900, 940);
	frameRate(30);
	camera.zoom = 0.5;
	textSize(width / 3);
	textAlign(CENTER, CENTER);
	let items = [];
	items.push(new item('Мел', 0.3, 0.03));
	items.push(new item('Сосед', 5, 0.1));
	items.push(new item('Машина', 20, 0.5));
	items.push(new item('Ракета', 50, 2));
	items.push(new item('Дудка', 140, 5));
	shopMark = new Shop(items);
	sounds.forEach((sound) => {
		sound.setVolume(0);
		sound.play();
		sound.loop();
	});
	for (var i = 0; i < 100; i++) {
		stars.push(new Star());
	}
}
(240 / 100 + 0.6) % 1;

function draw() {
	let H = ((Score / 10000 + 0.6) % 1) * 360;
	let L = 0.1;
	let S = 0.3;
	let r = 0,
		g = 0,
		b = 0;
	let C = (1 - Math.abs(2 * L - 1)) * S;
	let X = C * (1 - Math.abs((H / 60) % 2 - 1));
	let m = L - C / 2;
	if (H < 60) {
		r = (C + m) * 255;
		g = (X + m) * 255;
		b = (0 + m) * 255;
	} else if (H >= 60 && H < 120) {
		r = (X + m) * 255;
		g = (C + m) * 255;
		b = (0 + m) * 255;
	} else if (H >= 120 && H < 180) {
		r = (0 + m) * 255;
		g = (C + m) * 255;
		b = (X + m) * 255;
	} else if (H >= 180 && H < 240) {
		r = (0 + m) * 255;
		g = (X + m) * 255;
		b = (C + m) * 255;
	} else if (H >= 240 && H < 300) {
		r = (X + m) * 255;
		g = (0 + m) * 255;
		b = (C + m) * 255;
	} else if (H >= 300) {
		r = (C + m) * 255;
		g = (0 + m) * 255;
		b = (X + m) * 255;
	}
	speed = 10 + perSec;
	background(r, g, b);
	for (var i = 0; i < stars.length; i++) {
		stars[i].show();
	}
	noStroke();
	Cat.goToFrame(frame);
	animation(Cat, width / 2, height / 2 - 400);
	fill(230);
	textSize(width / 12);
	text('Insomnia score: ' + Score.toFixed(2), width / 2, 900);
	fill(180);
	textSize(width / 30);
	if (shopMark.items.length > 0) {
		perSec = 0;
		for (let i = 0; i < shopMark.items.length; i++) {
			perSec += shopMark.items[i].profit * shopMark.items[i].count;
			sounds[i].setVolume(shopMark.items[i].count / 100);
		}
	}
	text('Points per second: ' + perSec.toFixed(2), width / 2, 1030);
	if (time == 30) {
		time = -1;
		Score += perSec;
	}
	time++;
	let color = 180;
	fill(50, 60, 180);
	if (mouseX > width / 2 - 100 && mouseX < width / 2 + 100 && mouseY > 810 && mouseY < 850) {
		color = 255;
		fill(100, 100, 200);
		if (mouseIsPressed) {
			isShowVisible = true;
		}
	}
	rect(width / 2 - 200, 1150, 400, 80, 20);
	fill(color);
	textSize(width / 40);
	text('shop', width / 2, 1190);
	shopMark.draw();
	PrefMouseIsPressed = mouseIsPressed;
}

function mousePressed() {
	if (
		mouseX > width / 2 - Cat.getWidth() / 4 &&
		mouseX < width / 2 + Cat.getWidth() / 4 &&
		mouseY > height / 2 - Cat.getHeight() / 4 - 200 &&
		mouseY < height / 2 + Cat.getHeight() / 4 - 200
	) {
		if (frame === 0) {
			frame = 1;
		} else {
			frame = 0;
		}
		if (!isShowVisible) Score += 0.01;
	}
}

function mouseReleased() {
	if (
		mouseX > width / 2 - Cat.getWidth() / 4 &&
		mouseX < width / 2 + Cat.getWidth() / 4 &&
		mouseY > height / 2 - Cat.getHeight() / 4 - 200 &&
		mouseY < height / 2 + Cat.getHeight() / 4 - 200
	) {
		if (frame === 1) {
			frame = 0;
		} else {
			frame = 1;
		}
	}
}
const zeroPad = (num, places) => {
	var zero = places - num.toString().length + 1;
	return Array(+(zero > 0 && zero)).join('0') + num;
};
class item {
	constructor(name, price, profit) {
		this.name = name;
		this.count = 0;
		this.profit = profit;
		this.price = price;
	}
}

class Shop {
	constructor(items) {
		this.ExitColor = {
			r: 200,
			b: 20,
			g: 20
		};
		this.items = items;
	}
	draw() {
		if (isShowVisible) {
			fill(30, 30, 40);
			rect(width / 2 - 1000, -550, 2000, 1800, 100);
			fill(20, 20, 30);
			rect(width / 2 - 700, -550, 1400, 200, 100);
			fill(230);
			textSize(width / 18);
			text('shop', width / 2, -410);
			fill(this.ExitColor.r, this.ExitColor.g, this.ExitColor.b);
			rect(width / 2 + 860, -440, 80, 80, 30);
			textSize(width / 30);
			fill(200);
			text('x', width / 2 + 901, -402);
			this.ExitColor = {
				r: 200,
				b: 20,
				g: 20
			};
			if (mouseX > width / 2 + 860 / 2 && mouseX < width / 2 + 940 / 2 && mouseY > 15 && mouseY < 60) {
				this.ExitColor = {
					r: 250,
					b: 30,
					g: 30
				};
				if (mouseIsPressed) {
					isShowVisible = false;
				}
			}
			let position = 220;
			for (let i = 0; i < this.items.length; i++) {
				fill(10, 10, 20);
				if (
					mouseX > width / 2 - 800 / 2 &&
					mouseX < width / 2 + 800 / 2 &&
					mouseY > (position * i - 110) / 2 + 290 &&
					mouseY < position * i / 2 + 290
				) {
					fill(40, 40, 50);
					if (mouseIsPressed && !PrefMouseIsPressed && this.items[i].price <= Score) {
						this.items[i].count++;
						Score -= this.items[i].price;
						this.items[i].price *= 1.2;
					}
				}
				rect(width / 2 - 800, position * i, 1600, 110, 30);
				fill(230, 50, 50);
				textSize(width / 30);
				text(this.items[i].name, width / 2 - 600, position * i + 60);
				fill(200);
				textSize(width / 40);
				text(
					'в секунду: ' +
						this.items[i].profit +
						' | цена: ' +
						this.items[i].price.toFixed(2) +
						' | количество: ' +
						this.items[i].count,
					width / 2 + 300,
					position * i + 60
				);
			}
			fill(200);
			textSize(width / 20);
			text('Текущий счёт: ' + Score.toFixed(2), width / 2, -160);
		}
	}
}

class Star {
	constructor() {
		this.x = random(0, width);
		this.y = random(0, height);
		this.z = random(width);
		this.pz = this.z;
	}
	show() {
		this.z = this.z - speed;
		if (this.z < 1) {
			this.z = width;
			this.x = random(-width, width);
			this.y = random(-height, height);
			this.pz = this.z;
		}
		fill(255);
		noStroke();
		var sx = map(this.x / this.z, 0, 1, width / 2 - 200, width / 2 + 200);
		var sy = map(this.y / this.z, 0, 1, height / 2 - 200, height / 2 + 200);

		var r = map(this.z, 0, width, 20, 0);
		ellipse(sx, sy, r, r);

		var px = map(this.x / this.pz, 0, 1, width / 2 - 200, width / 2 + 200);
		var py = map(this.y / this.pz, 0, 1, height / 2 - 200, height / 2 + 200);

		this.pz = this.z;

		stroke(255);
		line(px, py, sx, sy);
	}
}
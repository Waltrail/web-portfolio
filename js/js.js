$(document).ready(function(){
    var cat = "Photography";
    var page = 1;
   $('.head-acord a').click(function(e){
       e.preventDefault();
       if ($(this).html() =="-"){
           $(this).html('+');
       }else{
           $(this).html('-');
       }
             
       var accord = "div[data-acord="+ $(this).attr('data-link') +"]"
       $(accord).toggleClass('acord-vis');
   });  
   
   var jsonss = function(categ, page){
        $.getJSON('js/gallery.json', function(data){
            var items = [];   
            var i = 0;
            $.each(data, function(key, val){
                if (val.categories == categ){
                    var keys = '<div class="portfolio wow zoomIn" data-wow-delay="'+ i +'s"><div class="picture" style="background-image: url(' + val.image + ')"><a href="#" class="lightbox"><i class="fa fa-expand" aria-hidden="true"></i></a><a href="'+ val.link +'"><i class="fa fa-external-link" aria-hidden="true"></i></a></div><div class="info"><p class="title">'+ val.title +'</p><p>'+ val.text +'</p></div></div>';
                    items.push(keys);
                    //Время задержки анимации
                    if (i <= 1.2){
                        i= i + 0.2;
                    }else{
                        i =0;    
                    }
                }
        });
            // $('.portfolios').html(items.join(''));
            $('.portfolios').html('');
            for (let index = (page-1) * 8; index < (page-1) * 8 + 8; index++) {
                $('.portfolios').append(items[index]);
                console.log(page);
            }


        });
   }


   jsonss(cat, 1);

  $('.sub-menu a').click(function(e){
        e.preventDefault();
        cat = $(this).attr('href');
        jsonss(cat, 1);
        $('.sub-menu a').removeClass('active');  
        $(this).addClass('active');

        $('.pages a').removeClass('active'); 
        $('.pages li:first-child a').addClass('active');
  });

  $('.pages a').click(function(e){
    e.preventDefault();
    var pages = parseInt($(this).attr('data-pages'));
    jsonss(cat, pages);
    $('.pages a').removeClass('active');  
    $(this).addClass('active');
});

    $(document).on('click', '.picture .lightbox' ,function(e){
        e.preventDefault();
        var img = $(this).parent();
        console.log(img.css('background-image')); 
    });



});
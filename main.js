function buildAd(file) {
    xhr.open("GET", file);
    xhr.responseType = "DOMString";
    xhr.send();
}


$('.head-acord a').click(e => {
    e.preventDefault();
    let head = $(e.currentTarget).parent().parent();
    let accordion = head.parent();
    let all = accordion.children('.acord').children('.body-acord');
    let child = head.children(".body-acord");
    let open = false;
    if (child.css('display') == 'none')
        open = true;
    all.parent().children('.head-acord').children('a').text('+');
    all.slideUp();
    if (open) {
        child.slideToggle(100);
        $(e.currentTarget).text('-');
    }
});

var items = [];
var page;
var nav = [];
$('.main-menu li div ul').hide();
$('.main-menu li div ul li ul').hide();

$('.main-menu>li>a').click(e => {
    $('.main-menu li div ul').fadeOut(200);
    e.preventDefault();
    let h = $(e.currentTarget).parent().children('div').children('ul');
    let state;
    if (h.css('display') == 'none') {
        state = false;
    } else {
        state = true;
    }
    $('.main-menu li div ul').fadeOut(200);
    if (!state) {
        h.fadeToggle(200);
    }
    $('.main-menu li div ul li ul').fadeOut(200);
});

$('.main-menu li div ul li a').click(e => {
    e.preventDefault();
    $(e.currentTarget).parent().children('ul').fadeToggle(200);
});

$('.SUBMENU ul li a').click(e => {
    e.preventDefault();
    page = 0;
    let a = $(e.currentTarget);
    let li = a.parent();
    let index = li.index();
    let data = ["Photography", "OldStyle", "Technology", "Lifestyle", "Wordpess", "Photoshop"];
    json = $.getJSON({
        'url': "posts.json",
        'async': false
    });
    li.parent().children('li').children('a').removeClass('selected');
    a.addClass('selected');
    json = JSON.parse(json.responseText);
    let List = $('.Portfolios');
    List.children('.PORTFOLIO').remove();
    items = [];
    for (let i = 0; i < json[data[index]].length; i++) {
        items.push("<div class = 'PORTFOLIO wow zoomIn' data-wow-delay='"+ i / 50 +"s'> <div class='picture' style='background-image: url(" + json[data[index]][i]['picture'] + ");'><a href='#'><i class='fa fa-expand'></i></a><a href='#'><i class='fa fa-external-link'></i></a></div><div class='info'><p class='title'>" + json[data[index]][i]["title"] + "</p><p>" + json[data[index]][i]["info"] + "</p></div></div>")
    }
    for(let i = page * 8; i<page*8+8; i++){
        List.append(items[i]);
    }
    nav = [];
    if(items.length / 8 > 1){
        nav.push("<li><a href='#' ><i class='fa fa-chevron-left'></i></a></li>");
        for(let i = 0; i < items.length/8; i++){
            nav.push("<li><a class='button' href='#'>"+ (i + 1) + "</a></li>");
        }
        nav.push("<li><a href='#' ><i class='fa fa-chevron-right'></i></a></li>");
    }        
    let buttons = $('.Navi ul li .button');
    let navi = $('.Navi ul');
    buttons.parent().parent().children('li').remove();
    for(let i = 0; i < nav.length; i++){
        navi.append(nav[i]);
    }
    buttons.eq(page).addClass('buttonActive');
});

$(document).on( 'click', '.Navi ul li a', e => {
    e.preventDefault();    
    let a = $(e.currentTarget);
    let index = a.parent().index();
    a.parent().parent().children('li').children('.button').removeClass('buttonActive');
    if(index == 0 || index == a.parent().parent().children('li').length - 1){
        if(index == 0){
            page--;
            if(page < 0){
                page = a.parent().length - 2;
            }
        }
        else{
            page++;
            if(page>3){
                page = 0;
            }
        }
        a.parent().parent().children('li').children('.button').eq(page).addClass('buttonActive');
    }
    else{
        a.addClass('buttonActive');
        page = index - 1;
    }
    let List = $('.Portfolios');
    List.children('.PORTFOLIO').remove();
    for(let i = page*8; i<page*8+8; i++){
        if(i < items.length){
            List.append(items[i]);
        }
    }
});

$('.PORTFOLIO .picture a').click(e => {
    e.preventDefault();
    let a = $(e.currentTarget);
    let index = a.index();
    if(index == 0){
        a.parent()
    }
});
function make2DArray(cols, rows) {
    let arr = new Array(cols);
    for (let i = 0; i < arr.length; i++) {
        arr[i] = new Array(rows);
    }
    return arr;
}
  
let grid;
let cols;
let rows;
let resolution = 40;
let PlayerField = [];
let EnemyField = [];
let numberOfShips = 10;
let Ships = [];

function setup() {
    createCanvas(900, 400);
    cols = (width - 100) / resolution/2;
    rows = height / resolution;
    PlayerField = make2DArray(cols,rows);
    EnemyField = make2DArray(cols,rows);    
    for(let i = 0; i < cols; i++){
        for(let j = 0; j < rows; j++){
            PlayerField[i][j] = new box(i*resolution,j*resolution,resolution,resolution,0);
        }
    }
    for(let i = 0; i < cols; i++){
        for(let j = 0; j < rows; j++){
            EnemyField[i][j] = new box(i*resolution + 500,j*resolution,resolution,resolution, 1);
        }
    }
}

function draw(){
    background(0);
    PlayerField.forEach(element => {
        element.forEach(elem => {
            elem.draw();
        });
    });
    EnemyField.forEach(element => {
        element.forEach(elem => {
            elem.draw();
        });
    });
    if(numberOfShips != 0){
        checkForUndrawables(PlayerField);
    }
}

const checkForUndrawables = (array) => {
    linker = 0;
    cruisers = 0;
    destroyers = 0;
    torpedoBoats = 0;
    for(let i = 0; i < cols - 1; i++){
        for(let j = 0; j < rows - 1; j++){
            let bool = false;
            if(array[i][j].state == 1){
                Ships.forEach(Ship => {
                    if(Ship.checkPosition(i,j)){
                        bool = true;
                    }
                });
                if(!bool){
                    let s = new ship(i,j);
                    Ships.push(s);
                }
            }           
        }
    }
    if(Ships.length > 0){
        Ships.forEach(Ship => {
            
        });
    }
    else{

    }
}

class box{    
    constructor(x,y, width, height, player){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.player = player // 0 - player, 1 - enemy
        this.state = 0; // -1 - undrawabel, 0 - empty, 1 - ship, 2 - nothing, 3 - fired
    }
    draw(){        
        if(this.player == 0){            
            if(mouseY >= this.y && mouseY <= this.y + this.height && mouseX >= this.x && mouseX <= this.x + this.width && mouseIsPressed && numberOfShips > 0 && this.state != 1 && this.state != -1){
                numberOfShips--;
                console.log(this.state);
                this.state = 1;
            }
        }   
        else{
            if(mouseY >= this.y && mouseY <= this.y + this.height && mouseX >= this.x && mouseX <= this.x + this.width && mouseIsPressed && this.state != 2 && this.state !=3){                
                console.log(PlayerField[(x-500)/resolution][y/resolution].state == 1);
                if(PlayerField[(x-500)/resolution][y/resolution].state == 1){
                    this.state = 3;
                }
                else{
                    this.state = 2;
                }
            }
        }
        
        if(this.state == 0){
            fill(250, 250, 255);
        }
        else if(this.state == 1){
            fill(30 ,30 ,250);
        }
        else if(this.state == 2){
            fill(30, 10, 10);
        }
        else{
            fill(250, 20, 20);
        }
        rect(this.x, this.y, this.width,this.height)
    }
}

class ship{
    constructor(x, y, direction = 2, width = 1){
        this.x = x;
        this.y = y;
        this.direction = direction; // 0 - right, 1 - down, 2>= - unknow
        this.width = width;
    }
    get Type(){
        return this.type;
    }
    checkPosition(i,j){
        if(this.direction == 0){
            let bool = false;
            for(let k = 0; k < width; k++){
                if(i == this.x + k && j == this.y){
                    bool = true;
                }
            }
            return bool;
        }
        else if(this.direction == 1){
            let bool = false;
            for(let k = 0; k < width; k++){
                if(i == this.x && j == this.y + k){
                    bool = true;
                }
            }
            return bool;
        }
        else{
            let bool = false;            
            if(i == this.x && j == this.y){
                bool = true;
            }
            
            return bool;
        }
        
    }
}
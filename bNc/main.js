//Globals
var isUpdate = false;
var gameEnd = true;
var nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
var guessNumbers = [];
var turns = "";
var cows = 0;
var bulls = 0;
var isInputCorrect = false;
var input = "";

const guessNewNumbers = () => {
  let shuffled = nums.sort(() => 0.5 - Math.random());
  guessNumbers = [];
  guessNumbers = shuffled.slice(0, 4);
};

const gameVsPC = () => {
  GetUserInput();
  CheckForBNC();
  CheckForEndGame();
};

const gameRun = () => {
  gameEnd = false;
  isUpdate = true;
  turns = "";
  guessNewNumbers();
  while (!gameEnd) {
    if (isUpdate) {
      gameVsPC();
    }
  }
  alert("Молодец ты прошёл игру, класс, здорово!");
  gameRun();
};

function CheckForEndGame() {
  turns += input + " | Коров: " + cows + ", быков: " + bulls + "\n";
  if (bulls == 4) {
    gameEnd = true;
  } else alert(turns);
}

function CheckForBNC() {
  cows = 0;
  bulls = 0;
  if (guessNumbers.some(substring => input.includes(substring))) {
    for (i = 0; i < 4; i++) {
      if (input.includes(guessNumbers[i])) {
        if (input.charAt(i) == guessNumbers[i]) {
          bulls++;
        } else {
          cows++;
        }
      }
    }
  }
}

var guesses = [];
const getGuesses = () => {
  
}

function GetUserInput() {
  isInputCorrect = false;
  do {
    input = prompt("Введите 4х значное число", "");
    if (
      input != null &&
      input.length == 4 &&
      !isNaN(input) &&
      !["-", "+", ".", ",", "e"].some(substring => input.includes(substring))
    ) {
      if (
        input[0] != input[1] &&
        input[0] != input[2] &&
        input[0] != input[2] &&
        input[0] != input[3] &&
        input[1] != input[2] &&
        input[1] != input[3] &&
        input[1] != input[0] &&
        input[2] != input[1] &&
        input[2] != input[0] &&
        input[2] != input[3] &&
        input[3] != input[0] &&
        input[3] != input[1] &&
        input[3] != input[2]
      ) {
        isInputCorrect = true;
      } else {
        alert("Вы ввели два или более одинаковых числа!");
      }
    } else {
      alert("Некорректный ввод!");
    }
  } while (!isInputCorrect);
}

const gameAi = () =>{ 
    guessNewNumbers()   
    gameEnd = false;
    isUpdate = true;
    turns = "";
    GetUserInput();
    do{
    CheckForBNC();
    alert(cows + ", " + bulls + ", " + guessNumbers + ", " + input);
    guessNewNumber();
    CheckForEndGame();
    }while(!gameEnd);
}

const guessNewNumber = () => {    
    
    guessNumbers = [];
    guessNumbers = shuffled.slice(0, 4);
    
};